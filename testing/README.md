# Unit Testing Javascript with Jasmine

> **Jasmine** is one of the popular JavaScript **unit testing** frameworks which is capable of **testing** synchronous and asynchronous JavaScript code. It is used in BDD (behavior-driven development) programming which focuses more on the business value than on the technical details. [--HowToDoInJava](https://howtodoinjava.com/scripting/javascript/jasmine-unit-testing-tutorial/)

Using the standalone version of jasmine 3.3.0, `SpecRunner.html` will run the tests/specs on the js files in `static/js`.
To see current test results, simply run `SpecRunner.html`.