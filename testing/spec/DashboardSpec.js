const allGatewayIdArray = ["112233", "223344", "334455", "445566", "556677", "667788", "778899", "889900","990011", "001122"]

describe("All Gateway Ids Array", function() {
  const correctSet = new Set(allGatewayIdArray)

  it("should return array of all gateway Ids when given all heartbeats", function() {
      const gatewayData = getAllGatewayJSON()

      const returnedSet = new Set(getAllGatewayIdArray(gatewayData))

      expect(returnedSet).toEqual(correctSet)
  })

  it("should return empty array when given no data/empty array", function() {
    const returnedSet = new Set(getAllGatewayIdArray([]))

    expect(returnedSet).toEqual(new Set())
  })
})

describe("Latest Sensor Data Array", function() {
  const gatewayData = getAllGatewayJSON()

  it("should return an empty array when asked for 0 entries", function() {
    const returnedArray = getLatestSensorDataArray(0, getAllGatewayJSON(), "112233", {sensorName: "Heartbeat", sensorType: "timestamp"})
    expect(returnedArray).toEqual([])
  })

  it("should return an empty array when given empty gatewayData array", function() {
    const returnedArray = getLatestSensorDataArray(5, [], "112233", {sensorName: "Heartbeat", sensorType: "timestamp"})
    expect(returnedArray).toEqual([])
  })

  const gateway112233HeartbeatArray = ["2018-11-08 23:45:18", "2018-11-08 23:45:19", "2018-11-08 23:45:19", "2018-11-08 23:45:19", "2018-11-08 23:45:20"]
  const gateway223344CPUArray = ["95%", "5%", "76%", "27%", "8%"]
  const gateway334455BatteryArray = ["52%", "26%", "53%", "77%", "20%"]
  const gateway556677MemoryArray = ["33%", "54%", "82%", "21%", "79%"]

  it("should return array of 5 length with gateway 112233's HEARTBEAT data", function() {
    const returnedArray = getLatestSensorDataArray(5, getAllGatewayJSON(), "112233", {sensorName: "Heartbeat", sensorType: "timestamp"})

    expect(returnedArray).toEqual(gateway112233HeartbeatArray)
  })

  it("should return array of 5 length with gateway 223344's CPU data", function() {
    const returnedArray = getLatestSensorDataArray(5, getAllGatewayJSON(), "223344", {sensorName: "CPU", sensorType: "diagnostic"})

    expect(returnedArray).toEqual(gateway223344CPUArray)
  })

  it("should return array of 5 length with gateway 334455's BATTERY data", function() {
    const returnedArray = getLatestSensorDataArray(5, getAllGatewayJSON(), "334455", {sensorName: "Battery", sensorType: "diagnostic"})

    expect(returnedArray).toEqual(gateway334455BatteryArray)
  })

  it("should return array of 5 length with gateway 556677's MEMORY data", function() {
    const returnedArray = getLatestSensorDataArray(5, getAllGatewayJSON(), "556677", {sensorName: "Memory", sensorType: "diagnostic"})

    expect(returnedArray).toEqual(gateway556677MemoryArray)
  })

})
