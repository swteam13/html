function getAllGatewayJSON() {
  const allGatewayResponse = [
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:18"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:18",
        "diagnostics":{
           "cpu":"95%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"52%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"92%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"76%",
           "memory":"33%",
           "battery":"39%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:19"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"62%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"0%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"2%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"22%",
           "memory":"69%",
           "battery":"29%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:19"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"5%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"26%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"30%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"46%",
           "memory":"54%",
           "battery":"48%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:19"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"91%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"43%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"88%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"47%",
           "memory":"64%",
           "battery":"29%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:19"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"76%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"53%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"8%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"75%",
           "memory":"82%",
           "battery":"39%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:19"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"85%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"14%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"51%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"24%",
           "memory":"4%",
           "battery":"22%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:19"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"27%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "battery":"77%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "memory":"57%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:19",
        "diagnostics":{
           "cpu":"52%",
           "memory":"21%",
           "battery":"68%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:20"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"93%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "battery":"32%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "memory":"41%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"54%",
           "memory":"9%",
           "battery":"88%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:20"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"8%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "battery":"20%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "memory":"35%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"54%",
           "memory":"79%",
           "battery":"57%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:20"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"66%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "battery":"24%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "memory":"89%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"88%",
           "memory":"53%",
           "battery":"93%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:20"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"5%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "battery":"30%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "memory":"54%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"29%",
           "memory":"20%",
           "battery":"93%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:20"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"17%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "battery":"79%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "memory":"61%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"28%",
           "memory":"76%",
           "battery":"35%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:20"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"65%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "battery":"78%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "memory":"74%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:20",
        "diagnostics":{
           "cpu":"84%",
           "memory":"1%",
           "battery":"13%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"90%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"68%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"41%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"63%",
           "memory":"32%",
           "battery":"24%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"11%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"10%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"0%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"63%",
           "memory":"21%",
           "battery":"77%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"33%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"83%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"5%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"16%",
           "memory":"90%",
           "battery":"95%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"63%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"85%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"79%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"3%",
           "memory":"41%",
           "battery":"19%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"5%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"30%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"40%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"100%",
           "memory":"41%",
           "battery":"45%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"74%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"61%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"22%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"70%",
           "memory":"61%",
           "battery":"15%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:21"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"12%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "battery":"23%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "memory":"88%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:21",
        "diagnostics":{
           "cpu":"25%",
           "memory":"58%",
           "battery":"54%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:22"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"24%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "battery":"17%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "memory":"54%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"8%",
           "memory":"48%",
           "battery":"61%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:22"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"56%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "battery":"0%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "memory":"44%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"62%",
           "memory":"50%",
           "battery":"91%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:22"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"83%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "battery":"98%"
        }
     },
     {
        "gatewayId":"445566",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "memory":"78%"
        }
     },
     {
        "gatewayId":"556677",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"84%",
           "memory":"64%",
           "battery":"49%"
        }
     },
     {
        "gatewayId":"667788",
        "timestamp":"2018-11-08 23:45:22"
     },
     {
        "gatewayId":"778899",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"43%"
        }
     },
     {
        "gatewayId":"889900",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "battery":"89%"
        }
     },
     {
        "gatewayId":"990011",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "memory":"56%"
        }
     },
     {
        "gatewayId":"001122",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"21%",
           "memory":"73%",
           "battery":"16%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-08 23:45:22"
     },
     {
        "gatewayId":"223344",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "cpu":"18%"
        }
     },
     {
        "gatewayId":"334455",
        "timestamp":"2018-11-08 23:45:22",
        "diagnostics":{
           "battery":"4%"
        }
     }
  ]

  return allGatewayResponse
}//end of getAllGatewayJSON

function getOneGatewayJSON() {
  const oneGatewayResponse = [
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "cpu":"20%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "battery":"58%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "memory":"5%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "cpu":"74%",
           "memory":"85%",
           "battery":"46%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "cpu":"67%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "battery":"37%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "memory":"8%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "cpu":"33%",
           "memory":"67%",
           "battery":"60%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "cpu":"26%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:07",
        "diagnostics":{
           "battery":"3%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"31%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"50%",
           "memory":"48%",
           "battery":"26%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"59%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"72%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"63%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"62%",
           "memory":"18%",
           "battery":"35%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"83%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"69%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"8%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"94%",
           "memory":"99%",
           "battery":"62%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"60%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"92%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"77%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"81%",
           "memory":"83%",
           "battery":"34%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"84%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"76%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"86%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"13%",
           "memory":"32%",
           "battery":"14%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"57%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"14%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"38%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"38%",
           "memory":"96%",
           "battery":"89%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"63%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"39%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"53%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"96%",
           "memory":"39%",
           "battery":"85%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"9%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "battery":"93%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "memory":"82%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:08",
        "diagnostics":{
           "cpu":"80%",
           "memory":"92%",
           "battery":"94%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"29%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"96%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"77%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"53%",
           "memory":"88%",
           "battery":"13%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"8%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"55%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"9%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"22%",
           "memory":"45%",
           "battery":"47%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"11%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"48%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"32%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"97%",
           "memory":"81%",
           "battery":"82%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"51%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"40%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"84%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"12%",
           "memory":"44%",
           "battery":"32%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"34%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"83%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"89%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"35%",
           "memory":"62%",
           "battery":"55%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"12%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"49%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"98%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"8%",
           "memory":"83%",
           "battery":"35%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"26%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "battery":"74%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "memory":"73%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"51%",
           "memory":"75%",
           "battery":"20%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:09",
        "diagnostics":{
           "cpu":"9%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"2%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"0%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"85%",
           "memory":"84%",
           "battery":"22%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"33%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"14%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"7%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"51%",
           "memory":"18%",
           "battery":"1%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"62%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"56%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"85%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"49%",
           "memory":"19%",
           "battery":"19%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"24%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"14%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"54%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"93%",
           "memory":"62%",
           "battery":"2%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"62%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"84%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"57%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"100%",
           "memory":"78%",
           "battery":"68%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"58%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"82%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"93%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"17%",
           "memory":"99%",
           "battery":"13%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"5%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"20%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "memory":"76%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"35%",
           "memory":"6%",
           "battery":"63%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10"
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "cpu":"39%"
        }
     },
     {
        "gatewayId":"112233",
        "timestamp":"2018-11-09 00:07:10",
        "diagnostics":{
           "battery":"63%"
        }
     }
  ]

  return oneGatewayResponse
}//end of getOneGatewayJSON
