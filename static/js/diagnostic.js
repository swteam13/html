function setHeader(gatewayId) {
    //display gateway daily history header
    document.getElementById('diagnostic-header').innerHTML = `Gateway ${gatewayId} Daily History`
}

//get all data and display it in a searchable, sortable table
function displayDailyHistoryTable(gatewayData) {
    let tableDataSet = getGatewayHistoryArray(gatewayData)

    //display table data using DataTable
    $('#diagnostic-history-table').DataTable( {
        data: tableDataSet,
        columns: [
            {title: "Sensor"},
            {title: "Time"},
            {title: "Data"}
        ]
    })
}//end of displayDailyHistoryTable

//make an an array of all of today's data for that gatewayId
//formatted in such a way that it can be displayed in a dataTable
function getGatewayHistoryArray(gatewayData) {
    let gatewayHistoryArray = []

    //for each gatewayEntry, check that it is an entry from today.
    //if entry has diagnostics, push all diagnostics data
    //else, only push timestamp as a heartbeat
    $.each(gatewayData, function (i, gatewayEntry) {
        const gatewayEntryTimeDateSet = timestampToTimeDateSet(gatewayEntry.timestamp)
        const currentTimeAndDateSet = getCurrentTimeDateSet()
        if(gatewayEntryTimeDateSet.date == currentTimeAndDateSet.date) {    //only display data from today
            const time = gatewayEntryTimeDateSet.time
            if(gatewayEntry.hasOwnProperty("diagnostics")){ //diagnostics included in gatewayEntry
                if(gatewayEntry.diagnostics.hasOwnProperty("cpu")) gatewayHistoryArray.push(["CPU", time, gatewayEntry.diagnostics.cpu])
                if(gatewayEntry.diagnostics.hasOwnProperty("memory")) gatewayHistoryArray.push(["Memory", time, gatewayEntry.diagnostics.memory])
                if(gatewayEntry.diagnostics.hasOwnProperty("battery")) gatewayHistoryArray.push(["Battery", time, gatewayEntry.diagnostics.battery])
                if(gatewayEntry.diagnostics.hasOwnProperty("os")) gatewayHistoryArray.push(["OS", time, gatewayEntry.diagnostics.os])
            } else {    //gateway entry was just a heartbeat
                gatewayHistoryArray.push(["Heartbeat", time, "-"])
            }
        }
    })

    return gatewayHistoryArray
}//end of getGatewayHistoryArray

function displayDownloadButton(gatewayId) {
    //code for button
    let downloadButtonHTML = `<a class="btn btn-primary" href="#" download>Download Daily Diagnostic</a>`

    //display download button
    document.getElementById('diagnostic-download').innerHTML = downloadButtonHTML
}

function displayTestDropdown(gatewayId) {
    const friendlyTestNamesArray = getAllFriendlyTestNamesArray()

    //open code for dropdown
    let dropdownHTML = `
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Run Test
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    `

    $.each(friendlyTestNamesArray, function (i, friendlyTestName) {
        dropdownHTML += `<button class="dropdown-item" data-toggle="modal" data-target="#testModal" onclick="runTest('${friendlyTestName}', ${gatewayId})">${friendlyTestName}</button>`
    })

    //close dropdown divs
    dropdownHTML += `
                        </div>
                    </div>
    `

    //display dropdownHTML
    document.getElementById('diagnostic-dropdown').innerHTML = dropdownHTML

    setScrollToTop();   //animate Back to Top button & smooth scrolling
}//end of displayTestDropdown

$(function() {
    const errorMsgId = 'diagnostic-header'

    let urlParams = parseURLParams(location.href)
    if(urlParams.length == 0 || !urlParams.hasOwnProperty("gatewayId")) {
        try {
            document.getElementById(errorMsgId).innerHTML = `Error, no gatewayId received!`
        }
        catch(e) {    //if can't display error in div, do it in console
            if (e instanceof TypeError) {
                console.log("Not running diagnostic.html! Testing?")
            }
            else {
                console.log("Error!!")
            }
        }
    } else {    //url header contains gatewayId
        let gatewayId = urlParams.gatewayId[0]

        setHeader(gatewayId)

        const gatewayURL = `https://${location.hostname}/api/gateway/${gatewayId}`
        // const gatewayURL = `http://localhost/test01/dbToJSON.php` //for testing
        // const gatewayURL = `https://localhost:3000/api/gateway/${gatewayId}` //for testing

        //using AJAX, receives JSON from URL in the form of the data var
        $.ajax({
            url: gatewayURL
        }).then(function(data) {//success
            if(data.length == 0) {//display error message
                document.getElementById(errorMsgId).innerHTML = `Error, no data received!`
            } else {
                displayDailyHistoryTable(data)
                // displayDownloadButton(gatewayId) //not ready for demo
                displayTestDropdown(gatewayId)
            }
        }, function() {//fail
            //display error message
            document.getElementById(errorMsgId).innerHTML = `Error, could not connect! Gateway ${gatewayId}, URL: ${gatewayURL}`
        }) //end of then
    }//end of else: url header contains gatewayId
}) //end of doc ready