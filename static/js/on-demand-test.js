//display test name in the modal title/header
function setModalHeader(testName) {
    document.getElementById('modalTitleLabel').innerHTML = `${testName} Results`
}

function resetTestResultBody() {
    let originalHTML = `<br><br><div class="text-center"><i class="fas fa-circle-notch fa-spin fa-3x"></i><br><br>Running test, please wait...</div><br><br>`
    document.getElementById('test-results-body').innerHTML = originalHTML
}

function friendlyTestNameToTestFileName(friendlyTestName) {
    const fileNamesArray = getAllTestFileNamesSet()

    let i = 0
    while (i <= fileNamesArray.length) {
        let fileNameSet = fileNamesArray[i]
        if(friendlyTestName == fileNameSet.friendlyName) {
            return fileNameSet.filename
        }
        i++
    }

    //could not find match
    return "-1"
}//end of friendlyTestNameToTestFileName

//post/request system to run test for gatewayId. If request is successful,
//store requestedTime and return true/start trying to request test results.
//if request fails, display error message & return false/don't request test results
function startTest(startTestURL, requestResultsURL, testFileName, gatewayId) {
    const requestPayload = {"filename": testFileName, "gatewayId": gatewayId}
    $.post(startTestURL, requestPayload, function(testTicket, success, xhr) {
        if(xhr.status == 201){  //201: request was successful
            if(testTicket.length == 0) {//request successful but no data given
                displayTestResults(`Error, did not receive response! Gateway: ${gatewayId}, filename: ${testFileName}, URL: ${startTestURL}`)
            } else {//request successful! now try to find test results
                const requestedTime = testTicket.requestedTime
                pollForTestResults(requestedTime, requestResultsURL)
            }
        }
    }) //end of $.post
        .fail(function(xhr) {
            if(xhr.status == 412) {  //412: something went wrong
                displayTestResults(`Error, could not request test! Gateway: ${gatewayId}, filename: ${testFileName}, URL: ${startTestURL}`)
            }
        })//end of $.fail
}//end of startTest

//wrap test results in pre and code tags to display text exactly as given
function displayTestResults(testResults) {
    let testResultsHTML = `<code>${testResults}</code>`
    document.getElementById('test-results-body').innerHTML = testResultsHTML
}

let pollFunction = null
//send post request every 1 sec for 60 secs or
//until data is successfully received & stored in testResults global var
function pollForTestResults(requestedTime, URL) {
    //poll function every second
    pollFunction = setInterval(() => requestTestResults(requestedTime, URL), 1*1000);

    // after 60 seconds, stop
    setTimeout(() => { clearInterval(pollFunction); }, 60*1000);


    // pollFunc(requestTestResults(requestedTime, URL), 60*1000, 1*1000);  //parameters: function, timeout(ms), interval(ms)

    // const receivedTestResults = testResults != ''
    // if(!receivedTestResults) {    //no data was received
    //     displayTestResults("Error, try again shortly.")
    // }
}

//using requestedTime as a unique identifier, request test results.
//if received test results, store it in testResults global var and return true
//else, test results not ready yet, return false
function requestTestResults(requestedTime, URL) {
    $.post(URL, requestedTime, function (data, success, xhr) {
        if(xhr.status == 201) { //201: received test results!
            clearInterval(pollFunction)    //stop polling!

            let originalHTML = `<br><br><div class="text-center"><i class="fas fa-circle-notch fa-spin fa-3x"></i><br><br>Received Test Result! Refreshing...</div><br><br>`
            document.getElementById('test-results-body').innerHTML = originalHTML

            setTimeout(function(){ window.location.replace(location.href); }, 5000);

            // testResults = JSON.stringify(data)
            // displayTestResults(testResults)
        }
    })//end of $.post
}

function getAllTestFileNamesSet() {
    const fileNamesArray = [
        {friendlyName: "Battery Check", filename: "batteryTest.py"},
        // {friendlyName: "CPU Test", filename: "cpuTest.py"},
        // {friendlyName: "Current Timestamp", filename: "dateTime.py"},
        // {friendlyName: "Health Test", filename: "healthTest.py"},
        {friendlyName: "Memory Test", filename: "memoryTest.py"},
        {friendlyName: "OS Check", filename: "osCheck.py"},
        // {friendlyName: "Ping Test", filename: "pingTest2.py"}
    ]

    return fileNamesArray
}//end of getAllTestFileNamesSet

function getAllFriendlyTestNamesArray() {
    let friendlyTestNamesArray = []
    $.each(getAllTestFileNamesSet(), function (testKey, fileNameSet) {
        friendlyTestNamesArray.push(fileNameSet.friendlyName)
    })

    return friendlyTestNamesArray
}//end of getAllFriendlyTestNamesArray

//using AJAX, get results from test and display, as is, into modal body
function runTest(friendlyTestName, gatewayId) {
    setModalHeader(friendlyTestName)
    resetTestResultBody()
    $('#test-modal').modal('show')  //show loading screen

    const testFileName = friendlyTestNameToTestFileName(friendlyTestName)
    if(testFileName == "-1") { //did not find filename
        displayTestResults(`Error, test name is incorrect! Test Name: ${friendlyTestName}`)
    } else {    //send filename to CiC & display results
        const startTestURL = `https://${location.hostname}/api/gateway/diagnostic` //post: ask to run test in this URL
        const requestResultsURL = `https://${location.hostname}/api/gateway/diagnosticInformation` //post: ask for test results

        //post request to start test & if successful, post request to receive test results
        startTest(startTestURL, requestResultsURL, testFileName, gatewayId)
    }//end of else: send filename to CiC & display results
}