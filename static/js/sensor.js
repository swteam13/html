function setPageHeader(sensorName) {
    //display sensor header
    document.getElementById('sensor-header').innerHTML = `${sensorName} History`
    //update title to reflect sensor name
    document.title = `${sensorName} History - Team 13`
}

function displaySensorHistoryTable(gatewayData, gatewayId, sensorName) {
    let tableDataSet = []
    let sensorHistoryArray = getSensorHistoryArray(gatewayData, gatewayId, sensorName)

    let columnSet = []
    //set columnSet depending what sensor data is expected
    if(sensorName == "Heartbeat") {
        columnSet = [
            {title: "Timestamp"}
        ]
    } else {    //sensor is a diagnostic
        columnSet = [
            {title: "Timestamp"},
            {title: "Data"}
        ]
    }

    $.each(sensorHistoryArray, function(i, sensorHistoryEntry) {
        let tableDataRow = []
        if(columnSet.length > 1) {  //true: it's a diagnostic
            tableDataRow = [sensorHistoryEntry.timestamp, sensorHistoryEntry.data]
            tableDataSet.push(tableDataRow)
        } else { //false: it's a heartbeat
            tableDataRow = [sensorHistoryEntry.timestamp]
            tableDataSet.push(tableDataRow)
        }
    })

    //display table data using DataTable
    $('#sensor-history-table').DataTable( {
        data: tableDataSet,
        columns: columnSet
    })
}//end of displaySensorHistoryTable

function  getSensorHistoryArray(gatewayData, gatewayId, sensorName) {
    let sensorHistoryArray = []

    $.each(gatewayData, function(i, gatewayEntry) {
        if(sensorName != "Heartbeat") {  //true: the data we're looking for is a diagnostic
            if(gatewayEntry.hasOwnProperty("diagnostics")) {//true: find correct info and push into sensorHistoryArray
                switch(sensorName) {
                    case "CPU":
                        if(gatewayEntry.diagnostics.hasOwnProperty("cpu")) sensorHistoryArray.push({timestamp: gatewayEntry.timestamp, data: gatewayEntry.diagnostics.cpu});
                        break;
                    case "Battery":
                        if(gatewayEntry.diagnostics.hasOwnProperty("battery")) sensorHistoryArray.push({timestamp: gatewayEntry.timestamp, data: gatewayEntry.diagnostics.battery});
                        break;
                    case "Memory":
                        if(gatewayEntry.diagnostics.hasOwnProperty("memory")) sensorHistoryArray.push({timestamp: gatewayEntry.timestamp, data: gatewayEntry.diagnostics.memory});
                        break;
                    case "OS":
                        if(gatewayEntry.diagnostics.hasOwnProperty("os")) sensorHistoryArray.push({timestamp: gatewayEntry.timestamp, data: gatewayEntry.diagnostics.os});
                        break;
                }//end of switch
            }//end of if(gatewayEntry.hasOwnProperty("diagnostics"))
        } else { //the data we're looking for is the heartbeat/timestamp
            sensorHistoryArray.push({timestamp: gatewayEntry.timestamp})
        }
    })//end of each

    return sensorHistoryArray
}//end of getSensorHistoryArray

$(function () {
    const errorMsgId = 'sensor-header'

    let urlParams = parseURLParams(location.href)
    if(urlParams.length == 0 || !urlParams.hasOwnProperty("gatewayId")) {
        try {
            document.getElementById(errorMsgId).innerHTML = `Error, no gatewayId received!`
        }
        catch(e) {    //if can't display error in div, do it in console
            if (e instanceof TypeError) {
                console.log("Not running sensor.html! Testing?")
            }
            else {
                console.log("Error!!")
            }
        }
    } else {    //url header contains gatewayId
        let gatewayId = urlParams.gatewayId[0]
        let sensorName = urlParams.sensorName[0]

        setPageHeader(sensorName)

        const gatewayURL = `https://${location.hostname}/api/gateway/${gatewayId}`
        // const gatewayURL = `https://localhost:3000/api/gateway/${gatewayId}` //for testing

        //using AJAX, recieves JSON from URL in the form of the data var
        $.ajax({    //get all data for a specific gateway
            url: gatewayURL
        }).then(function(data) {//success
            displaySensorHistoryTable(data, gatewayId, sensorName)
        }, function() {//fail
            //display error message
            document.getElementById(errorMsgId).innerHTML = `Error, could not connect! URL: ${gatewayURL}`
        }) //end of then
    }//end of else: url header contains gatewayId
}) //end of doc ready