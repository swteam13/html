//check if username and password are alphanumeric and if passwords match
//if not, display error messages
function passedErrorCheckRegister(errorMsgId, username, password, passwordConfirm) {
    let errArray = []
    if($.trim(username) == '') {//error: username filled with  spaces
        errArray.push("Invalid username. Username filled with spaces.")
    }
    if($.trim(password) == '' || $.trim(passwordConfirm) == '') {//error: password(s) filled with spaces
        errArray.push("Invalid password(s). Password(s) filled with spaces.")
    }
    if(!RegExp("^[a-zA-Z0-9]*$").test(username)) {//error: username is not alphanumeric
        errArray.push("Invalid username. Only alphanumeric usernames allowed.")
    }
    if(!(RegExp("^[a-zA-Z0-9]*$").test(password) && RegExp("^[a-zA-Z0-9]*$").test(passwordConfirm))) {//error: passwords are not alphanumeric
        errArray.push("Invalid password(s). Only alphanumeric passwords allowed.")
    }
    if(password != passwordConfirm) {//error: passwords don't match
        errArray.push("Passwords do not match.")
    }

    //toString array
    if(errArray.length != 0) {
        let errorMsgString = errArray[0]
        if(errArray.length > 1) {  //if more than one error add br tag between errors
            for(let i = 1; i < errArray.length; i++) {
                errorMsgString += '<br>' + errArray[i]
            }
        }
        //display error message string in html
        document.getElementById(errorMsgId).innerHTML = errorMsgString
    }
    const result = errArray.length == 0  //check if any errors were saved in errArray
    return result
}//end of passedErrorCheckRegister

//jQuery to load AFTER doc is loaded
$(function(){
    //set form variable
    var form = $('#register-form')

    //"hijack" form submit button
    form.submit(function(event){
        event.preventDefault() //prevent default form action

        //clear error messages
        const errorMsgId = 'register-error-message'
        document.getElementById(errorMsgId).innerHTML = ''

        const username = $('#register-username').val()
        const password = $('#register-password').val()
        const passwordConfirm = $('#register-password-confirm').val()

        //client-end error checking
        if(passedErrorCheckRegister(errorMsgId, username, password, passwordConfirm)) {

            // //testing
            // if(username == 'user1') {
            //     document.getElementById(errorMsgId).innerHTML = 'Username is not available.'
            // }
            // else {//registered successfully, show login form
            //     $('#register-username').val('') //clear username input field
            //     $('#register-password').val('') //clear password input field
            //     $('#register-password-confirm').val('') //clear password confirm input field
            //     toggleLoginRegister()   //display login form
            //     document.getElementById("register-success-message").innerHTML = 'Registered Successfully!'
            // }
            // //end of testing

            //process AJAX request
            const URL = `https://${location.hostname}/api/users/register`
            const userData = {
                username: username,
                password: password
            }
            $.post(URL, userData, function(data, success, xhr) {
                if(xhr.status == 201) {  //201: no errors, user added, redirect to login
                    $('#register-username').val('') //clear username input field
                    $('#register-password').val('') //clear password input field
                    $('#register-password-confirm').val('') //clear password confirm input field
                    toggleLoginRegister()   //show login form
                    document.getElementById("register-success-message").innerHTML = 'Registered Successfully!'
                } else {  //username is not available
                    document.getElementById(errorMsgId).innerHTML = 'Username is not available.'
                }
            }) //end of $.post
                .fail(function(xhr) {
                    if(xhr.status == 412) {  //412: something went wrong
                        document.getElementById(errorMsgId).innerHTML = `Error, could not post! URL: ${URL}`
                    }
                })//end of $.fail


        } //end of if: passedErrorCheckLogin
    }) //end of form.submit: "hijack" form submit button
}) //end of doc ready