function toggleLoginRegister() {
    var login = document.getElementById("login-div")
    var register = document.getElementById("register-div")
    if(login.style.display === "none") { //show login/hide register
        login.style.display = "block"
        register.style.display = "none"

        //update title to reflect content change
        document.title = 'Login - Team 13'

    } else {  //show register/hide login
        login.style.display = "none"
        register.style.display = "block"

        //update title to reflect content change
        document.title = 'Register - Team 13'
    }
}
