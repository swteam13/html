//get all gatewayId's using a set to make sure that
//every set.add() is adding a unique gatewayId
//then, convert to an array for usability
function getAllGatewayIdArray(gatewayData) {
    let gatewayIdSet = new Set()
    $.each(gatewayData, function(i, gatewayEntry) {
        gatewayIdSet.add(gatewayEntry.gatewayId)
    })

    const gatewayIdArray = [...gatewayIdSet]    //convert set into array
    return gatewayIdArray
}//end of getAllGatewayIdArray

//for each gateway in gatewayId Array, create HTML that will display
//in div#gateways
function displayGateways(gatewayData, gatewayIdArray) {
    let gatewaysHTML = '';

    for(index in gatewayIdArray) {  //add gateway and its sensors' HTML to gatewaysHTML
        let gatewayId = gatewayIdArray[index]
        gatewaysHTML += getGatewayHTML(gatewayData, gatewayId)

        if(gatewayIdArray[parseInt(index)+1] != undefined) {    //there is a gateway to display after this one
            gatewaysHTML += '<hr class="hr-gateway">'   //seperate gateways w/ a hr
        }
    }//end of for:add gateway and its sensors' HTML to gatewaysHTML

    //display gatewaysHTML
    document.getElementById('gateways').innerHTML = gatewaysHTML
}//end of displayGateways

//return HTML of gateway header, on-demand diagnostics button,
//and sensor cards for each sensor plugged into gateway
function getGatewayHTML(gatewayData, gatewayId) {
    //add Gateway Header and On-Demand Diagnostics Button code to gatewayHTML
    let gatewayHTML = `
                <div class="gateway">
                    <div class="row align-items-start">
                        <h2 id="gateway${gatewayId}" class="col">Gateway ${gatewayId}</h2>
                        <div id="gateway-diagnostic" class="col-sm-auto">
							<form action="diagnostic.html" method="get">
								<button class="btn btn-diagnostic" type="submit" name="gateway-${gatewayId}-diagnostic">Daily Diagnostic</button>
								<input type="hidden" id="gatewayId" name="gatewayId" value="${gatewayId}">
							</form>
                        </div>
                    </div>
    `

    gatewayHTML += getGatewaySensorsHTML(gatewayData, gatewayId)

    //close div.gateway
    gatewayHTML += `
                </div>
    `

    return gatewayHTML
}//end of getGatewayHTML

//create cards for all the sensors on the gateway
function getGatewaySensorsHTML(gatewayData, gatewayId) {
    const sensorKeys = {//Id sensor types, to see if it needs to drill down to diagnostics key in JSON object
        "Heartbeat" : "timestamp",
        "CPU" : "diagnostic",
        "Battery" : "diagnostic",
        "Memory" : "diagnostic",
        "OS": "diagnostic"
    }

    //initialize sensor html by opening div.row
    let gatewaySensorsHTML = '<div class="row">'

    //for each sensor, attempt to get HTML for sensor card
    //if response == 0, then no data was available to create HTML
    //otherwise, add created HTML to gatewayHTML
    $.each(sensorKeys, function(sensorName, sensorType) {
        let sensorCardTextHTML = getSensorCardTextHTML(gatewayData, gatewayId, {sensorName: sensorName, sensorType: sensorType})
        if(sensorCardTextHTML != "0") {   //0: sensor not found, did not create sensorCardTextHTML
            //open card code
            gatewaySensorsHTML += `
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-body">
                                <h3>${sensorName}</h3>
                                <hr class="hr-cards">
            `

            gatewaySensorsHTML += sensorCardTextHTML    //append card-text

            //close card code
            gatewaySensorsHTML += `
                                <div class="text-center">
                                    <form action="sensor.html" method="get">
                                        <button class="btn btn-primary btn-chevron-down" type="submit" name="${sensorName}-submit">
                                            <i class="fas fa-chevron-down fa-lg" aria-hidden="true"></i>
                                            <input type="hidden" id="gatewayId" name="gatewayId" value="${gatewayId}">
                                            <input type="hidden" id="sensorName" name="sensorName" value="${sensorName}">
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            `
        }//end of if(sensorCardTextHTML != "0")
    })//end of each

    gatewaySensorsHTML += '</div>'  //close div.row

    return gatewaySensorsHTML
}//end of getGatewaySensorsHTML

//creates HTML for  n entries of sensor data to show as card text
function getSensorCardTextHTML(gatewayData, gatewayId, sensorKey) {
    let sensorCardTextArray = getLatestSensorDataArray(5, gatewayData, gatewayId, sensorKey)
    if(sensorCardTextArray.length == 0) return "0"  //no data for that sensor/sensor not found
    let sensorCardTextHTML = ''
    $.each(sensorCardTextArray, function (i, sensorData) {
        sensorCardTextHTML += `<p class="card-text">${sensorData}</p>`
    })

    return sensorCardTextHTML
}//end of getSensorCardTextHTML

//loop through each entry in the gatewayData and find numOfEntries amount of sensor data
//that matches the gatewayId and the sensorName (from the sensorKey {sensorName: sensorType})
//return array of latest data for that specific sensor
function getLatestSensorDataArray(numOfEntries, gatewayData, gatewayId, sensorKey) {
    let sensorDataArray = []

    //loop through the gatewayData until you have numOfEntries amount of data in the sensorDataArray
    //or until you reach the end of gatewayData
    let index = 0
    while(sensorDataArray.length != numOfEntries && index < gatewayData.length) {
        let gatewayEntry = gatewayData[index]

        if(gatewayEntry.gatewayId == gatewayId) {//use data from matching gatewayId
            //if sensor data is from diagnostics, make sure that diagnostics exists in gatewayEntry before continuing
            if(sensorKey.sensorType == "diagnostic" && gatewayEntry.hasOwnProperty("diagnostics")) {
                //find/push the value that matches the diagnostics data/sensorName that you're looking for
                switch (sensorKey.sensorName) {
                    case "CPU":
                        if(gatewayEntry.diagnostics.hasOwnProperty("cpu")) sensorDataArray.push(gatewayEntry.diagnostics.cpu);
                        break;
                    case "Battery":
                        if(gatewayEntry.diagnostics.hasOwnProperty("battery")) sensorDataArray.push(gatewayEntry.diagnostics.battery);
                        break;
                    case "Memory":
                        if(gatewayEntry.diagnostics.hasOwnProperty("memory")) sensorDataArray.push(gatewayEntry.diagnostics.memory);
                        break;
                    case "OS":
                        if(gatewayEntry.diagnostics.hasOwnProperty("os")) sensorDataArray.push(gatewayEntry.diagnostics.os)
                        break;
                }//end of switch
            } else if(sensorKey.sensorType == "diagnostic") {
                //if sensor data is from diagnostic, but there are no diagnostics in gatewayEntry,
                //break and return empty sensorDataArray
                break
            } else {//sensorType == "timestamp", push timestamp value
                sensorDataArray.push(gatewayEntry.timestamp)
            }//end of else: sensorType == "timestamp", push timestamp value
        }//end of if: use data from matching gatewayId
        index += 1//increment index
    }//end of while-loop

    return sensorDataArray
}//end of getLatestSensorDataArray

//
function displayGatewayDropdown(gatewayIdArray) {
    //open code for dropdown
    let dropdownHTML = `
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Gateways
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    `

    $.each(gatewayIdArray, function (i, gatewayId) {
        dropdownHTML += `<a class="dropdown-item" href="#gateway${gatewayId}">Gateway ${gatewayId}</a>`
    })

    //close dropdown divs
    dropdownHTML += `
                        </div>
                    </div>
    `

    //display dropdownHTML
    document.getElementById('gateway-dropdown').innerHTML = dropdownHTML

    setScrollpy();  //using scrollpy, smooth dropdown scrolling
    setScrollToTop();   //animate Back to Top button & smooth scrolling
}

$(function(){

    const errorMsgId = 'gateways'
    //using AJAX, recieves JSON from URL in the form of the data var
    const URL = `https://${location.hostname}/api/gateway/heartbeats` //get all heartbeats & diagnostic data
    // const URL = `https://localhost:3000/api/gateway/heartbeats` //for testing
    // const URL = `http://localhost/test01/dbToJSON.php` //for testing

    $.ajax({//get all heartbeats & diagnostic data
        url: URL
    }).then(function(data) {//success
        if(data.length == 0) {//display error message
            document.getElementById(errorMsgId).innerHTML = `Error, no data received!`
        }else {
            let gatewayIdArray = getAllGatewayIdArray(data)

            displayGateways(data, gatewayIdArray)   //display gateways and their sensors
            displayGatewayDropdown(gatewayIdArray)  //dropdown to easily navigate to a specific gateway
        }
    }, function() {//fail
        //display error message
        document.getElementById(errorMsgId).innerHTML = `Error, could not connect! URL: ${URL}`
    }) //end of then
})//end of doc ready