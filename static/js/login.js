//check if there's errors and display error messages
function passedErrorCheckLogin(errorMsgId, username, password) {
    let errArray = []
    if($.trim(username) == '') {//error: username filled with  spaces
        errArray.push("Invalid username. Username filled with spaces.")
    }
    if($.trim(password) == '') {//error: password filled with spaces
        errArray.push("Invalid password. Password filled with spaces.")
    }
    if(!RegExp("^[a-zA-Z0-9]*$").test(username)) {//error: username is not alphanumeric
        errArray.push("Invalid username. Only alphanumeric usernames allowed.")
    }
    if(!RegExp("^[a-zA-Z0-9]*$").test(password)) {//error: password is not alphanumeric
        errArray.push("Invalid password. Only alphanumeric passwords allowed.")
    }

    //toString & display error messages
    if(errArray.length != 0) {
        let errorMsgString = errArray[0]
        if(errArray.length > 1) {  //if more than one error add br tag between errors
            for(let i = 1; i < errArray.length; i++) {
                errorMsgString += '<br>' + errArray[i]
            }
        }
        //display error message string in html
        document.getElementById(errorMsgId).innerHTML = errorMsgString
    }
    const result = errArray.length == 0  //check if any errors were saved in errArray
    return result
}//end of passedErrorCheckLogin

//jQuery to load AFTER doc is loaded
$(function(){
    //set form variable
    var form = $('#login-form')

    //"hijack" form submit button
    form.submit(function(event){
        event.preventDefault()  //prevent default form action

        //clear error messages
        const errorMsgId = 'login-error-message'
        document.getElementById(errorMsgId).innerHTML = ''

        const username = $('#login-username').val()
        const password = $('#login-password').val()

        //client-end error checking
        if(passedErrorCheckLogin(errorMsgId, username, password)) {

            // //testing
            // if(username != "user1" || password != "123") {
            //     document.getElementById(errorMsgId).innerHTML = 'Invalid username or password.'
            // }
            // else{
            //     window.location.replace("./dashboard.html")
            // }
            // //end of testing


            //send username and password to CiC for authentication
            const URL = `https://${location.hostname}/api/users/login`
            const userData = {
                username: username,
                password: password
            }

            $.post(URL, userData, function(data, success, xhr) {
                if(xhr.status == 201) {  //authenticated user, reload index to show heartbeatTable
                    window.location.replace("./dashboard.html")
                }
            }) //end of $.post
            .fail(function(xhr) {
                if(xhr.status == 400) {  //400: invalid credentials, ask user to re-enter username/password
                    document.getElementById(errorMsgId).innerHTML = 'Invalid username or password.'
                }
                else if(xhr.status == 412) {  //412: something's gone wrong
                    document.getElementById(errorMsgId).innerHTML = `Error, could not connect! URL: ${URL}`
                }
            })//end of $.fail
        } //end of if: passedErrorCheckLogin
    }) //form.submit
}) //doc ready