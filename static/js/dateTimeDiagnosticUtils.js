function getCurrentTimeDateSet() {
    return timestampToTimeDateSet(getCurrentTimestamp())
}

//split timestamp into date and time array
function timestampToTimeDateSet(timestamp) {
    const timeDateArray = timestamp.split(' ')   //["2018-10-06", "22:40"]
    const timeDateSet = { date: timeDateArray[0], time: timeDateArray[1] }

    return timeDateSet
}//end of timestampToTimeDateSet

function getCurrentTimestamp() {
    const currentDate = new Date()

    const year = currentDate.getFullYear()  //no formatting needed, year will (hopefully) always be 4 digits
    const date = formatDateTimeString(currentDate.getDate().toString())
    const month = formatDateTimeString((currentDate.getMonth() + 1).toString()) //Jan is 0 not 1, so add 1

    const hour = formatDateTimeString(currentDate.getHours().toString())
    const minute = formatDateTimeString(currentDate.getMinutes().toString())

    const dateTimeString = `${year}-${month}-${date} ${hour}:${minute}`
    return dateTimeString
}//end of getCurrentTimestamp

//format string to match "yyyy-mm-dd hh:mm" format:
//month: 1-12 to 01-12
//date: 1-31 to 01-31
//hour: 0-23 to 00-23
//minute: 0-59 to 00-59
function formatDateTimeString(dateTimeString) {
    //if string is length 1, add 0 to beginning & return
    //else, return string as is
    return dateTimeString.length == 1 ? "0" + dateTimeString : dateTimeString
}